package ro.siit.curs7;

public class MainAppClass {

    public static void main(String[] args) throws IllegalArgumentException, ArithmeticException {
//        throwMyException();
//        System.out.println("Un mesaj");
        try
        {
            System.out.println(circleArea(-5));
        } catch (IndexOutOfBoundsException e) {
            System.out.println("index out of bounds");
        }
        catch (Exception e) {
            System.out.println("Other exceptions");
        }
        finally {
            System.out.println("This block will always run!!!!" );
        }

        //System.out.println("ceva");
        try {
            int age = Integer.parseInt(args[0]);
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("There are no arguments !! ");
        }

        try {
            System.out.println(circleArea(-5));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("final !!");

        try {
            newThrowException();
        }
        catch (MyCustomException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void throwMyException() {

        throw new IllegalArgumentException("Aici am aruncat exceptia pentru ca ....");
    }

    private static double circleArea(double radius) throws Exception {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius must be positive value, your value is " + radius);
        }
        return Math.PI * radius * radius;
    }

    private static void newThrowException() throws MyCustomException {
        throw new MyCustomException("This  is my custom exception!!!", -200);

    }

}


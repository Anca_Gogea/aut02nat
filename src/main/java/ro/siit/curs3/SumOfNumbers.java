package ro.siit.curs3;

public class SumOfNumbers {

    public static int ComputeSum(int n) {
        int sum = 0;
        for (int i = 0 ; i <= n; i++) {
            sum += i;
        }
        return  sum;
    }

    public static int ComputeSumFormula(int n) {
        return n * (n + 1) / 2;
    }
}


package ro.siit.curs5;
public class Teacher extends PersonC5 {
    private String discipline;
    private int profID;

    public Teacher(String name, String address, String cnp, int age, String gender) {
        super(name, address, cnp, age, gender);
    }

    public Teacher(String name, String address, String cnp, int age, String gender, String discipline, int profID) {
        super(name, address, cnp, age, gender);
        this.discipline = discipline;
        this.profID = profID;

    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public int getProfID() {
        return profID;
    }

    public void setProfID(int profID) {
        this.profID = profID;
    }

    @Override
    public String toString() {
        return super.toString() +
                ",discipline='" + discipline + '\'' +
                ", profID=" + profID;
    }
}


package ro.siit.curs5;
import java.awt.*;

public class Triangle extends Shape{

    public Triangle(Color color) {
        super(5, color);
    }

    @Override
    public void draw() {
        System.out.println("Draw a triangle...");
    }

    public void draw(String info){
        System.out.println(info);
        this.draw();
    }
    public void draw(Integer info){
        System.out.println(info);
        this.draw();
    }
}
